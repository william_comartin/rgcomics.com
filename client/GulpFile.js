var pkg = require('./package.json'),
    gulp = require('gulp'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    concat = require('gulp-concat'),
    source = require('vinyl-source-stream'),
    connect = require('gulp-connect'),
    sftp = require('gulp-sftp'),
    templateCache = require('gulp-angular-templatecache'),
    livereload = require('gulp-livereload'),
    gettext = require('gulp-angular-gettext'),
    rsync = require('gulp-rsync');

gulp.task('js', function(){

  gulp.src('./app/shared/**/*.js')
    .pipe(sourcemaps.init())
    .pipe(concat('shared.js'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./dist/js'))
    .pipe(livereload());

  gulp.src('./app/components/**/*.js')
    .pipe(sourcemaps.init())
    .pipe(concat('components.js'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./dist/js'))
    .pipe(livereload());

  gulp.src(['./app/index.js'])
    .pipe(sourcemaps.init())
    .pipe(concat('app.js'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./dist/js'))
    .pipe(livereload());

  gulp.src(pkg.bowerFiles.javascript)
    .pipe(concat('vendor.js'))
    .pipe(gulp.dest('./dist/js'))
    .pipe(livereload());

});

gulp.task('templates', function(){

  gulp.src(['./app/components/**/*.html',
            './app/shared/**/*.html'])
    .pipe(templateCache('templates.js', {
      standalone: true
    }))
    .pipe(gulp.dest('./dist/js'))
    .pipe(livereload());

});

gulp.task('css', function(){

  // Application CSS Files
  gulp.src('./app/**/*.scss')
    .pipe(sourcemaps.init())
      .pipe(sass({ style: 'compressed' }))
    .pipe(concat('app.css'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./dist/css/'))
    .pipe(livereload());

  // Vendor CSS Files
  gulp.src(pkg.bowerFiles.stylesheets)
    .pipe(concat('vendor.css'))
    .pipe(gulp.dest('./dist/css/'))
    .pipe(livereload());

});

gulp.task('sync', function () {

  gulp.src('./app/index.html')
    .pipe(gulp.dest('./dist'))
    .pipe(livereload());

  gulp.src('./app/images/**/*.*')
    .pipe(gulp.dest('./dist/images'))
    .pipe(livereload());

  gulp.src('./bower_components/font-awesome/fonts/*.*')
    .pipe(gulp.dest('./dist/fonts'))
    .pipe(livereload());

  gulp.src('./app/fonts/**/*.*')
    .pipe(gulp.dest('./dist/fonts'))
    .pipe(livereload());

});

gulp.task('connect', function() {

  connect.server({
    root: './dist',
    // livereload: true,
    port: 8000
  });
});

gulp.task('rsync_staging', function() {
  gulp.src('dist')
    .pipe(rsync({
      root: 'dist',
      recursive: true,
      hostname: pkg.staging.host,
      destination: pkg.staging.remote_path,
      // clean: true,
      username: 'root',
      progress: true
    }));
});


gulp.task('watch', function () {
  livereload.listen();
  gulp.watch(['./app/**/*.js'], ['js']);
  gulp.watch(['./app/**/*.html'], ['templates']);
  gulp.watch(['./app/**/*.scss'], ['css']);
  gulp.watch(['./app/index.html',
              './app/images/**/*.*'], ['sync']);
});

gulp.task('build', ['js', 'templates', 'css', 'sync']);

gulp.task('default', ['build', 'connect', 'watch']);
gulp.task('stage', ['build', 'rsync_staging']);
// gulp.task('deploy', ['translations', 'js', 'css', 'sync', 'sftp_production']);
